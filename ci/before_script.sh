#!/usr/bin/env bash

set -e
set -x
mkdir -p /root/.cache/unity3d
mkdir -p /root/.local/share/unity3d/Unity/
set +x

# readonly base_dir="${CIRCLE_WORKING_DIRECTORY/\~/$HOME}"
# readonly unity_project_full_path="$base_dir/$PARAM_PROJECT_PATH"

unity_license_destination=/root/.local/share/unity3d/Unity/Unity_lic.ulf
android_keystore_destination="/$unity_project_full_path/keystore.keystore"


upper_case_build_target=${BUILD_TARGET^^};

echo "target received as $upper_case_build_target"

if [ "$upper_case_build_target" = "ANDROID" ]
then
    keystore_env="ANDROID_KEYSTORE_BASE64${KEYSTORE_CODE_NO}"
    echo "KEYSTORE USED: $keystore_env"
    echo ${!keystore_env}
    # nameref code =$keystore_env
    # eval "nameref code=$keystore_env"
    # echo "codeIS:$code"
    if [ -n ${!keystore_env} ]
    then
        echo "'$keystore_env' found, decoding content into ${android_keystore_destination}"
        echo ${!keystore_env} | base64 --decode > ${android_keystore_destination}
    else
        echo '${!keystore_env}'" env var not found, building with Unity's default debug keystore"
    fi
else
    echo "Keystore not required! Not an android build"
fi

if [ -n "$UNITY_LICENSE" ]
then
    echo "Writing '\$UNITY_LICENSE' to license file ${unity_license_destination}"
    echo "${UNITY_LICENSE}" | tr -d '\r' > ${unity_license_destination}
else
    echo "'\$UNITY_LICENSE' env var not found"
fi
